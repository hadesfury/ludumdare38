A Small War

![screen001.png](///raw/6c8/z/20cf.png)

Crush your opponent by spawning the good unit on the right lane... 

You can only build *light* unit at the beginning... but some tech are available!

Be aware that *light* units are weak against *medium* units, that *medium* unit are weak against *heavy* units and that *heavy* units are weak against *light* units


Control
-------
Esc: Pause 

S : Commit suicide

R : Reset view

Keyboard shorcut are available and displayed on action buttons

Links
-------
* **Windows:** https://www.dropbox.com/s/gvvkjlhzdabhr7s/Windows.zip?dl=0
* **OSX:** https://www.dropbox.com/s/y0bdat4vc99p9vx/OSX.zip?dl=0
* **Linux:** https://www.dropbox.com/s/4sopg20c3q2qols/Linux.zip?dl=0
* **Source Code:** https://bitbucket.org/hadesfury/ludumdare38

![screen000.png](///raw/6c8/z/21a1.png)

![screen003.png](///raw/6c8/z/21a4.png)

![screen004.png](///raw/6c8/z/21a6.png)

![screen005.png](///raw/6c8/z/21a8.png)

![screen006.png](///raw/6c8/z/21a9.png)

![screen007.png](///raw/6c8/z/21aa.png)

![Screen Shot 04-25-17 at 03.20 AM.PNG](///raw/6c8/z/20d6.png)

![Screen Shot 04-25-17 at 03.21 AM.PNG](///raw/6c8/z/21dd.png)

![Screen Shot 04-25-17 at 03.20 AM 001.PNG](///raw/6c8/z/21e2.png)