﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.Assertions;

public class GameStateManager : MonoBehaviour
{
    public enum GameState
    {
        MENU,
        START_GAME,
        INGAME,
        PAUSE_MENU,
        GAME_OVER
    }

    public static GameStateManager gameStateManager;

    private GameState currentState = GameState.MENU;

    private void Awake()
    {
        gameStateManager = this;
    }

    public GameState GetCurrentGameState()
    {
        return currentState;
    }

    private void Update()
    {
        OnUpdateState( currentState );
    }

    public void ChangeState(GameState par_new_game_state)
    {
        Assert.AreNotEqual(currentState, par_new_game_state);

        OnExitState(currentState);
        currentState = par_new_game_state;
        OnEnterState(currentState);
    }

    private void OnEnterState( GameState par_game_state)
    {
        switch (par_game_state)
        {
            case GameState.START_GAME:
            {
                GameManager.gameManager.Initialise();
                break;
            }
        }
    }

    private void OnExitState( GameState par_game_state)
    {

    }

    private void OnUpdateState(GameState par_game_state)
    {
        switch (par_game_state)
        {
            case GameState.MENU:
            {
                if (Input.GetKeyUp(KeyCode.Space))
                {
                    ChangeState(GameState.START_GAME);
                }
                break;
            }
            case GameState.START_GAME:
            {
                ChangeState(GameState.INGAME);
                break;
            }
            case GameState.INGAME:
            {
                // Done in GuiManager
                //if (Input.GetKeyUp(KeyCode.Escape))
                //{
                //    ChangeState(GameState.PAUSE_MENU);
                //}
                break;
            }
            case GameState.PAUSE_MENU:
            {
                if (Input.GetKeyUp(KeyCode.Escape))
                {
                    ChangeState(GameState.INGAME);
                }
                break;
            }
            case GameState.GAME_OVER:
            {
                if (Input.GetKeyUp(KeyCode.Space))
                {
                    ChangeState(GameState.MENU);
                }
                break;
            }
            default:
            {
                throw new ArgumentOutOfRangeException();
            }
        }
    }
}