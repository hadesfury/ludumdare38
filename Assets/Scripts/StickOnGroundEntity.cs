﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StickOnGroundEntity : MonoBehaviour
{
    [SerializeField] private Planet planet;
    [SerializeField] private Transform entityGroundNode;

    protected void Initialise(Planet par_planet)
    {
        planet = par_planet;
    }

    protected void StickOnground()
    {
        entityGroundNode.transform.localPosition = Vector3.forward * planet.GetPlanetRadius();
    }

    protected void Rotate180()
    {
        entityGroundNode.transform.localRotation = Quaternion.Euler(0, 0, 180);
    }

    public Vector3 GetPosition()
    {
        return entityGroundNode.position;
    }

    public void SetPosition(Vector3 par_transform_position)
    {
        Quaternion loc_destination_rotation = Quaternion.LookRotation(par_transform_position);

        transform.rotation = loc_destination_rotation;
    }

    protected Vector3 GetPositionOnGround(Vector3 par_normalised_destination)
    {
        return par_normalised_destination * planet.GetPlanetRadius();
    }
}