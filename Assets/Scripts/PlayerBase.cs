﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBase : StickOnGroundEntity
{
    [SerializeField] private List<Transform> spawnPointTable;

    private Player ownerPlayer;

    public void Initialise( Player par_player )
    {
        GetComponent<LifeComponent>().Initialise();

        ownerPlayer = par_player;
        gameObject.SetActive( true );
    }

    public void SpawnUnitOnLane( int par_lane_index, UnitType par_unit_type )
    {
        GameManager loc_game_manager = GameManager.gameManager;
        Transform loc_spawn_point = spawnPointTable[ par_lane_index ];
        AttackLane loc_attack_lane = loc_game_manager.GetAttackLane( loc_spawn_point );

        if ( loc_attack_lane.CanSpawn( par_unit_type, loc_spawn_point, ownerPlayer ) )
        {
            GameUnit loc_game_unit = loc_game_manager.GetNewGameUnit( par_unit_type );

            loc_game_unit.Initialise( this, loc_game_manager.GetPlanet() );
            loc_attack_lane.Spawn( loc_game_unit, loc_spawn_point, ownerPlayer );
        }
    }

    public int GetLaneCount()
    {
        return spawnPointTable.Count;
    }

    public Player GetOwnerPlayer()
    {
        return ownerPlayer;
    }

    public List<int> GetAttackedLaneIndexTable()
    {
        int loc_current_lane_index = 0;
        List<int> loc_attacked_lane_index_table = new List<int>();

        foreach ( Transform loc_spawn_point in spawnPointTable )
        {
            GameManager loc_game_manager = GameManager.gameManager;
            AttackLane loc_attack_lane = loc_game_manager.GetAttackLane(loc_spawn_point);

            if (loc_attack_lane.IsAttacked(loc_spawn_point))
            {
                loc_attacked_lane_index_table.Add( loc_current_lane_index );
            }

            ++loc_current_lane_index;
        }

        return loc_attacked_lane_index_table;
    }
}