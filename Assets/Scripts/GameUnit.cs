﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class GameUnit : StickOnGroundEntity
{
    public enum UnitState
    {
        MOVING,
        ATTACKING_BASE,
        ATTACKING_GAME_UNIT,
        WAITING,
        NONE
    }

    [SerializeField] private float movementSpeed = 1.0f;
    [SerializeField] private float attackPower = 2.0f;
    [SerializeField] private Transform meshDock;

    private Quaternion destination;

    private PlayerBase playerBase;
    private Battle currentBattle;
    private UnitState currentState = UnitState.NONE;
    private UnitType unitType;
    private MeshAnimation meshAnimation;

    public void Initialise(PlayerBase par_player_base, Planet par_planet)
    {
        base.Initialise(par_planet);

        playerBase = par_player_base;
    }

    private void Update()
    {
        StickOnground();

        if (playerBase == GameManager.gameManager.GetHumanPlayer().GetPlayerBase())
        {
            Rotate180();
        }

        if (GameStateManager.gameStateManager.GetCurrentGameState() == GameStateManager.GameState.INGAME)
        {
            if (currentState == UnitState.MOVING)
            {
                float loc_angle_to_destination = Quaternion.Angle(transform.rotation, destination);

                meshAnimation.SetIsAnimationPaused(false);

                if (loc_angle_to_destination > 0.1f)
                {
                    Quaternion loc_new_rotation = Quaternion.Lerp(transform.rotation, destination, (movementSpeed * Time.deltaTime) / loc_angle_to_destination);

                    transform.rotation = loc_new_rotation;
                }
                else
                {
                    currentState = UnitState.NONE;
                }
            }
            else
            {
                meshAnimation.SetIsAnimationPaused(true);
            }
        }
    }

    public bool IsEnemy(GameUnit par_other_unit)
    {
        return GetPlayerBase() != par_other_unit.GetPlayerBase();
    }

    public PlayerBase GetPlayerBase()
    {
        return playerBase;
    }

    public void SetUnitType(UnitType par_unit_type, Transform par_mesh_transform)
    {
        unitType = par_unit_type;
        par_mesh_transform.SetParent(meshDock, false);
        meshAnimation = par_mesh_transform.GetComponent<MeshAnimation>();
    }

    public UnitType GetUnitType()
    {
        return unitType;
    }

    public void SetBattle(Battle par_battle)
    {
        if (par_battle != null)
        {
            Assert.IsNull(currentBattle);

            if (par_battle.IsBaseBattle())
            {
                Assert.AreNotEqual(currentState, UnitState.ATTACKING_BASE);
                currentState = UnitState.ATTACKING_BASE;
            }
            else
            {
                Assert.AreNotEqual(currentState, UnitState.ATTACKING_GAME_UNIT);
                currentState = UnitState.ATTACKING_GAME_UNIT;
            }

            currentBattle = par_battle;
        }
        else
        {
            currentBattle = null;
            currentState = UnitState.NONE;
        }
    }

    public void Attack(GameUnit par_opponent)
    {
        float loc_damage_modifier = GameManager.gameManager.GetDamageModifier(GetUnitType(), par_opponent.GetUnitType());
        par_opponent.GetComponent<LifeComponent>().Attack(attackPower * (1 + loc_damage_modifier));

        //Debug.LogFormat("{0} <color=red>hit</color> {1}", name, par_opponent.name);
    }

    public void Attack(PlayerBase par_opponent_base)
    {
        par_opponent_base.GetComponent<LifeComponent>().Attack(attackPower);

        //Debug.LogFormat("{0} <color=cyan>hit</color> {1}", name, par_opponent_base.name);
    }

    public bool IsDead()
    {
        return GetComponent<LifeComponent>().IsDead();
    }

    public UnitState GetCurrentState()
    {
        return currentState;
    }

    public void MoveTo(Quaternion par_destination)
    {
        currentState = UnitState.MOVING;
        destination = par_destination;
    }
}