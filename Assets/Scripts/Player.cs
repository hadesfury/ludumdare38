﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

public abstract class Player : MonoBehaviour
{
    [SerializeField] protected PlayerBase playerBase;

    private float resourceCount = 0;
    private float currentUpgradeCost;
    private bool isMediumUnitUnlocked = false;
    private bool isHeavyUnitUnlocked = false;

    public virtual void Initialise()
    {
        resourceCount = 0;
        isMediumUnitUnlocked = false;
        isHeavyUnitUnlocked = false;

        currentUpgradeCost = GameManager.gameManager.GetInitialUpgradeCost();
    }

    public void SpawnUnitOnLane( int par_lane_index, UnitType par_unit_type )
    {
        playerBase.SpawnUnitOnLane( par_lane_index, par_unit_type );
    }

    public bool IsDead()
    {
        return playerBase.GetComponent<LifeComponent>().IsDead();
    }

    public PlayerBase GetPlayerBase()
    {
        return playerBase;
    }

    public float GetResourceCount()
    {
        return resourceCount;
    }

    public void AddResources( float par_resources_added )
    {
        resourceCount += par_resources_added;
    }

    public void RemoveResources( float par_resources_removed)
    {
        resourceCount -= par_resources_removed;
    }

    private void IncreaseUpgradeCost()
    {
        currentUpgradeCost *= GameManager.gameManager.GetUpgradeCostIncreaseFactor();
    }

    public void UnlockMediumUnit()
    {
        if (currentUpgradeCost <= resourceCount)
        {
            isMediumUnitUnlocked = true;

            RemoveResources(currentUpgradeCost);

           IncreaseUpgradeCost();
        }
    }

    public void UnlockHeavyUnit()
    {
        if (currentUpgradeCost <= resourceCount)
        {
            isHeavyUnitUnlocked = true;

            RemoveResources(currentUpgradeCost);

            IncreaseUpgradeCost();
        }
    }

    public bool GetIsMediumUnitUnlocked()
    {
        return isMediumUnitUnlocked;
    }

    public bool GetIsHeavyUnitUnlocked()
    {
        return isHeavyUnitUnlocked;
    }

    public float GetCurrentUpgradeCost()
    {
        return currentUpgradeCost;
    }
}