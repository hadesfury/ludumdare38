﻿using UnityEngine;
using System.Collections;

public class HumanPlayer : Player
{
    private void Update()
    {
        if (GameStateManager.gameStateManager.GetCurrentGameState() == GameStateManager.GameState.INGAME)
        {
            if (Input.GetKeyUp(KeyCode.S))
            {
                playerBase.GetComponent<LifeComponent>().Suicide();
            }
        }
    }
}
