﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MeshAnimation : MonoBehaviour
{
    [SerializeField] private List<Transform> meshTable;
    [SerializeField] private float animationSpeed = 1.0f;

    private int currentMeshIndex = 0;
    private float timeSinceLastFrameChange = 0;
    private bool isAnimationPaused = false;

    private void Awake()
    {
        foreach ( Transform loc_transform in meshTable )
        {
            loc_transform.gameObject.SetActive( false );
        }

        meshTable[ currentMeshIndex ].gameObject.SetActive( true );
    }

    private void Update()
    {
        if ( GameStateManager.gameStateManager.GetCurrentGameState() == GameStateManager.GameState.INGAME )
        {
            if ( !isAnimationPaused )
            {
                timeSinceLastFrameChange += Time.deltaTime;

                if ( timeSinceLastFrameChange >= animationSpeed )
                {
                    meshTable[ currentMeshIndex ].gameObject.SetActive( false );

                    currentMeshIndex = ( currentMeshIndex + 1 ) % meshTable.Count;

                    meshTable[ currentMeshIndex ].gameObject.SetActive( true );

                    timeSinceLastFrameChange = 0;
                }
            }
        }
    }

    public void SetIsAnimationPaused(bool par_is_animation_paused)
    {
        isAnimationPaused = par_is_animation_paused;
    }
}