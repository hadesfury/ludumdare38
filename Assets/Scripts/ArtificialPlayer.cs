﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Random = UnityEngine.Random;

public class ArtificialPlayer : Player
{
    [SerializeField] private float attackedSpawnRate = 1.0f;
    [SerializeField] private float spawnRate = 1.0f;

    private float timeSinceLastSpawn = 0;
    private float currentSpawnRate;

    public override void Initialise()
    {
        base.Initialise();

        currentSpawnRate = spawnRate;
    }

    private void Update()
    {
        if (GameStateManager.gameStateManager.GetCurrentGameState() == GameStateManager.GameState.INGAME)
        {
            timeSinceLastSpawn += Time.deltaTime;

            if (timeSinceLastSpawn >= spawnRate)
            {
                List<int> loc_attacked_lane_table = playerBase.GetAttackedLaneIndexTable();
                int loc_spawn_point_index = Random.Range(0, playerBase.GetLaneCount());
                UnitType loc_selected_unit_type = GetRandomAvailableUnitType();

                if (loc_attacked_lane_table.Count > 0)
                {
                    loc_spawn_point_index = Random.Range( 0, loc_attacked_lane_table.Count );
                    currentSpawnRate = attackedSpawnRate;
                }
                else
                {
                    currentSpawnRate = spawnRate;
                }

                SpawnUnitOnLane(loc_spawn_point_index, loc_selected_unit_type);

                float loc_upgrade_cost = GetCurrentUpgradeCost();

                if (loc_upgrade_cost <= GetResourceCount())
                {
                    int loc_random_value = Random.Range( 0, 2 );

                    if ( loc_random_value == 0 )
                    {
                        UnlockMediumUnit();
                    }
                    else
                    {
                        UnlockHeavyUnit();
                    }
                }

                timeSinceLastSpawn = 0;
            }
        }
    }

    private UnitType GetRandomAvailableUnitType()
    {
        UnitType loc_selected_unit_type;
        List<UnitType> loc_available_unit_type_table = new List<UnitType>();

        loc_available_unit_type_table.Add( UnitType.BLUE );

        if (playerBase.GetOwnerPlayer().GetIsMediumUnitUnlocked())
        {
            loc_available_unit_type_table.Add( UnitType.GREEN );
        }

        if (playerBase.GetOwnerPlayer().GetIsHeavyUnitUnlocked())
        {
            loc_available_unit_type_table.Add(UnitType.RED);
        }

        int loc_unit_type_index = Random.Range(0, loc_available_unit_type_table.Count);
        
        loc_selected_unit_type = loc_available_unit_type_table[loc_unit_type_index];

        return loc_selected_unit_type;
    }
}
