﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Planet : MonoBehaviour
{
    [SerializeField] private float planetRadius = 5.0f;
    [SerializeField] private Transform planetMesh;

    private void Awake()
    {
        planetMesh.transform.localScale = Vector3.one * planetRadius * 2.0f;
    }

    public float GetPlanetRadius()
    {
        return planetRadius;
    }
}