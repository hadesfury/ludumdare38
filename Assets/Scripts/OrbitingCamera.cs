﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrbitingCamera : MonoBehaviour
{
    [SerializeField] private Planet planet;
    [SerializeField] private float minOrbitingRadiusOffset = 0.5f;
    [SerializeField] private float maxOrbitingRadiusOffset = 10.0f;
    [SerializeField] private float orbitingSpeed = 10.0f;
    [SerializeField] private float mouseOrbitingSpeed = 0.3f;
    [SerializeField] private float zoomSpeed = 1.0f;
    [SerializeField] private float resetSpeed = 0.2f;
    [SerializeField] private Camera orbitingCamera;

    private bool
        resetting = false,
        panning = false;

    private Vector3
        previousMousePosition,
        newMousePosition,
        mouseDelta;

    private void Update()
    {
        float loc_horizontal_axis = Input.GetAxis( "Horizontal" ) + mouseDelta.x;
        float loc_vertical_axis = Input.GetAxis( "Vertical" ) + mouseDelta.y;
        float loc_mouse_scroll_wheel_axis = Input.GetAxis( "Mouse ScrollWheel" );

        if (resetting)
        {
            transform.localRotation = Quaternion.Lerp(transform.localRotation, Quaternion.identity, resetSpeed);

            if (Quaternion.Angle(transform.localRotation, Quaternion.identity) < 0.5f)
            {
                transform.localRotation = Quaternion.identity;
                resetting = false;
            }
        }
        else if ( Input.GetKeyUp( KeyCode.R ) )
        {
            resetting = true;
        }
        else
        {
            transform.Rotate( Vector3.up, -loc_horizontal_axis * Time.deltaTime * orbitingSpeed, Space.Self );
            transform.Rotate( Vector3.right, loc_vertical_axis * Time.deltaTime * orbitingSpeed, Space.Self );
        }
        
            UpdateZoomLevel( loc_mouse_scroll_wheel_axis );
            
        if (Input.GetMouseButtonDown(0) && (Input.touchCount != 2))
        {
            panning = true;
            previousMousePosition = Input.mousePosition;
        }
        if (panning)
        {
            newMousePosition = Input.mousePosition;
            mouseDelta = (previousMousePosition - newMousePosition) * mouseOrbitingSpeed;
            previousMousePosition = newMousePosition;
        }
        if (Input.GetMouseButtonUp(0))
        {
            panning = false;
            mouseDelta = Vector3.zero;
        }
    }

    private void UpdateZoomLevel(float par_zoom_direction)
    {
        Vector3 loc_previous_positon = orbitingCamera.transform.localPosition;

        if (par_zoom_direction > 0)
        {
            loc_previous_positon.z += Time.deltaTime * zoomSpeed;
        }
        else if (par_zoom_direction < 0)
        {
            loc_previous_positon.z -= Time.deltaTime * zoomSpeed;
        }

        loc_previous_positon.z = Mathf.Clamp(loc_previous_positon.z, -(planet.GetPlanetRadius() + maxOrbitingRadiusOffset), -(planet.GetPlanetRadius() + minOrbitingRadiusOffset));

        orbitingCamera.transform.localPosition = loc_previous_positon;
    }
}