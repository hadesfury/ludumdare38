﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LifeComponent : MonoBehaviour
{
    [SerializeField] private int initialLife = 10;
    [SerializeField] private float previousLife;
    [SerializeField] private Canvas lifeBarCanvas;
    [SerializeField] private Image lifeBarImage;

    private MeshRenderer mesh;
    private Material mat;
    private ParticleSystem damageParticles;
    private AudioSource audio;
    private Color emissiveColor;

    private float currentLife;

    private void Awake()
    {
        Initialise();
    }

    public void Initialise()
    {
        currentLife = initialLife;
        previousLife = initialLife;

        if (lifeBarCanvas != null)
        {
            lifeBarCanvas.gameObject.SetActive(false);
        }
    }

    private void Start()
    {
        mesh = GetComponentInChildren<MeshRenderer>(); // oui c'Est crado mais bon
        mat = mesh.material;
        emissiveColor = mat.GetColor ("_EmissionColor");

        damageParticles = GetComponentInChildren<ParticleSystem>();
        audio = GetComponentInChildren<AudioSource>();
    }

    public bool IsDead()
    {
        return currentLife <= 0;
    }

    public void Attack(float par_attack_power)
    {
        currentLife -= par_attack_power;

        if (currentLife <= 0)
        {
            Die();
        }
    }

    private void Die()
    {
        gameObject.SetActive( false );
    }

    public float GetCurrentLife()
    {
        return currentLife;
    }

    private void Update()
    {
        mat.SetColor ("_EmissionColor", emissiveColor * (currentLife/initialLife));

        if (currentLife != previousLife)
        {
            mat.SetColor ("_EmissionColor", emissiveColor * (currentLife/initialLife) * 2f);
            damageParticles.Emit(100);
            
            if (!audio.isPlaying)
            {
                audio.Play();
            }
        }

        previousLife = currentLife;

        if (lifeBarCanvas != null)
        {
            float loc_current_life_percentage = currentLife / initialLife;

            if (loc_current_life_percentage < 1)
            {
                lifeBarCanvas.gameObject.SetActive(true);
                lifeBarImage.fillAmount = loc_current_life_percentage;
            }
            else
            {
                lifeBarCanvas.gameObject.SetActive(false);
            }
        }
    }

    public void Suicide()
    {
        currentLife = 0;
    }
}
