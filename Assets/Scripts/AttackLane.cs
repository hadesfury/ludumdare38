﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Assertions;

public class AttackLane : MonoBehaviour
{
    [SerializeField] private Transform originAttackPoint;
    [SerializeField] private Transform originSpawnPoint;
    [SerializeField] private Transform destinationAttackPoint;
    [SerializeField] private Transform destinationSpawnPoint;
    [SerializeField] private int tileCount = 10;

    private readonly List<GameUnit> fromOriginTable = new List<GameUnit>();
    private readonly List<GameUnit> fromDestinationTable = new List<GameUnit>();

    private readonly Dictionary<int, GameUnit> tileTable = new Dictionary<int, GameUnit>();
    private readonly List<Battle> battleTable = new List<Battle>();

    public void Initialise()
    {
        fromOriginTable.Clear();
        fromDestinationTable.Clear();
        tileTable.Clear();
        battleTable.Clear();

        for (int loc_tile_index = 0; loc_tile_index < tileCount; loc_tile_index++)
        {
            tileTable.Add(loc_tile_index, null);
        }
    }

    public bool ContainsSpawnPoint(Transform par_spawn_point)
    {
        return par_spawn_point == originSpawnPoint || par_spawn_point == destinationSpawnPoint;
    }


    public bool CanSpawn( UnitType par_unit_type, Transform par_spawn_point, Player par_player )
    {
        bool loc_can_spawn_game_unit = false;
        int loc_spawn_point_index = GetSpawnPointIndex(par_spawn_point);
        float loc_unit_cost = GameManager.gameManager.GetUnitCost();
        float loc_resource_count = par_player.GetResourceCount();

        if ((tileTable[loc_spawn_point_index] == null) 
            && (loc_unit_cost <= loc_resource_count)
            && ((par_unit_type != UnitType.GREEN) || par_player.GetIsMediumUnitUnlocked())
            && ((par_unit_type != UnitType.RED) || par_player.GetIsHeavyUnitUnlocked())
        )
        {
            loc_can_spawn_game_unit = true;
        }

        return loc_can_spawn_game_unit;
    }

    public void Spawn(GameUnit par_game_unit, Transform par_spawn_point, Player par_player)
    {
        Assert.IsTrue(CanSpawn(par_game_unit.GetUnitType(), par_spawn_point, par_player));

        int loc_spawn_point_index = GetSpawnPointIndex(par_spawn_point);
        float loc_unit_cost = GameManager.gameManager.GetUnitCost();

        Assert.IsNull(tileTable[loc_spawn_point_index]);

        par_game_unit.SetPosition(par_spawn_point.position);

        if (par_spawn_point == originSpawnPoint)
        {
            fromOriginTable.Add(par_game_unit);
        }
        else if (par_spawn_point == destinationSpawnPoint)
        {
            fromDestinationTable.Add(par_game_unit);
        }

        tileTable[loc_spawn_point_index] = par_game_unit;

        par_player.RemoveResources(loc_unit_cost);

        //Debug.LogFormat("<color=green>Spawn {0}</color> on {1}", par_game_unit.name, par_spawn_point.name);
    }

    private int GetSpawnPointIndex(Transform par_spawn_point)
    {
        int loc_spawn_point_index = -1;

        if (par_spawn_point == originSpawnPoint)
        {
            loc_spawn_point_index = 0;
        }
        else if (par_spawn_point == destinationSpawnPoint)
        {
            loc_spawn_point_index = tileCount - 1;
        }

        return loc_spawn_point_index;
    }

    private int GetAttackPointIndex(Transform par_spawn_point)
    {
        int loc_attack_point_index = -1;

        if (par_spawn_point == originSpawnPoint)
        {
            loc_attack_point_index = 1;
        }
        else if (par_spawn_point == destinationSpawnPoint)
        {
            loc_attack_point_index = tileCount - 2;
        }

        return loc_attack_point_index;
    }

    private int GetEnemySpawnPointIndex(Transform par_spawn_point)
    {
        int loc_spawn_point_index = -1;

        if (par_spawn_point == destinationSpawnPoint)
        {
            loc_spawn_point_index = 0;
        }
        else if (par_spawn_point == originSpawnPoint)
        {
            loc_spawn_point_index = tileCount - 1;
        }

        return loc_spawn_point_index;
    }

    private int GetEnemyAttackPointIndex(Transform par_spawn_point)
    {
        int loc_attack_point_index = -1;

        if (par_spawn_point == destinationSpawnPoint)
        {
            loc_attack_point_index = 1;
        }
        else if (par_spawn_point == originSpawnPoint)
        {
            loc_attack_point_index = tileCount - 2;
        }

        return loc_attack_point_index;
    }

    public void Progress(float par_delta_time)
    {
        Progress(fromOriginTable, originSpawnPoint);
        Progress(fromDestinationTable, destinationSpawnPoint);

        List<Battle> loc_battle_to_remove = new List<Battle>();

        foreach (Battle loc_battle in battleTable)
        {
            loc_battle.Update(Time.deltaTime);

            if (loc_battle.IsOver())
            {
                loc_battle.Clear();
                loc_battle_to_remove.Add(loc_battle);
            }
        }

        foreach (Battle loc_battle in loc_battle_to_remove)
        {
            battleTable.Remove(loc_battle);
        }

        List<int> loc_tile_to_empty = new List<int>();

        foreach (KeyValuePair<int, GameUnit> loc_key_value_pair in tileTable)
        {
            GameUnit loc_current_game_unit = loc_key_value_pair.Value;

            if ((loc_current_game_unit != null) && (loc_current_game_unit.IsDead()))
            {
                loc_tile_to_empty.Add(loc_key_value_pair.Key);

                if (fromOriginTable.Contains(loc_current_game_unit))
                {
                    fromOriginTable.Remove(loc_current_game_unit);
                }

                if (fromDestinationTable.Contains(loc_current_game_unit))
                {
                    fromDestinationTable.Remove(loc_current_game_unit);
                }
            }
        }

        foreach (int loc_tile_to_empty_index in loc_tile_to_empty)
        {
            tileTable[loc_tile_to_empty_index] = null;
        }
    }

    private void Progress(List<GameUnit> par_game_unit_table, Transform par_spawn_point)
    {
        foreach (GameUnit loc_game_unit in par_game_unit_table)
        {
            switch (loc_game_unit.GetCurrentState())
            {
                case GameUnit.UnitState.ATTACKING_BASE:
                {
                    int loc_enemy_base_spawn_point_index = GetEnemySpawnPointIndex(par_spawn_point);

                    if (tileTable[loc_enemy_base_spawn_point_index] != null)
                    {
                        EndBaseBattle(loc_game_unit);
                        StartUnitBattle(loc_game_unit, tileTable[loc_enemy_base_spawn_point_index]);
                    }

                    break;
                }
                case GameUnit.UnitState.NONE:
                {
                    int loc_current_tile_index = GetUnitTileIndex(loc_game_unit);
                    int loc_next_tile_index = GetNextTileIndex(loc_current_tile_index, par_spawn_point);
                    int loc_enemy_base_attack_point_index = GetEnemyAttackPointIndex(par_spawn_point);

                    if (loc_current_tile_index == loc_enemy_base_attack_point_index)
                    {
                        int loc_enemy_base_spawn_point_index = GetEnemySpawnPointIndex(par_spawn_point);

                        if (tileTable[loc_enemy_base_spawn_point_index] == null)
                        {
                            StartBaseBattle(loc_game_unit);
                        }
                        else
                        {
                            StartUnitBattle(loc_game_unit, tileTable[loc_enemy_base_spawn_point_index]);
                        }

                    }
                    else if (IsNextTileEmpty(loc_current_tile_index, par_spawn_point))
                    {
                        tileTable[loc_current_tile_index] = null;
                        tileTable[loc_next_tile_index] = loc_game_unit;

                        loc_game_unit.MoveTo(GetTilePosition(loc_next_tile_index));
                    }
                    else
                    {
                        GameUnit loc_next_tile_occupier = tileTable[loc_next_tile_index];

                        if (loc_game_unit.IsEnemy(loc_next_tile_occupier))
                        {
                            StartUnitBattle(loc_game_unit, loc_next_tile_occupier);
                        }
                        //else
                        //{
                        //    //Waiting
                        //}
                    }

                    break;
                }

            }
        }
    }

    private Quaternion GetTilePosition(int par_tile_index)
    {
        Quaternion loc_tile_position;

        if (par_tile_index == 0)
        {
            loc_tile_position = Quaternion.LookRotation(originSpawnPoint.position);
        }
        else if (par_tile_index == 1)
        {
            loc_tile_position = Quaternion.LookRotation(originAttackPoint.position);
        }
        else if (par_tile_index == tileCount - 2)
        {
            loc_tile_position = Quaternion.LookRotation(destinationAttackPoint.position);
        }
        else if (par_tile_index == tileCount - 1)
        {
            loc_tile_position = Quaternion.LookRotation(destinationSpawnPoint.position);
        }
        else
        {
            Quaternion loc_origin_rotation = Quaternion.LookRotation(originAttackPoint.position);
            Quaternion loc_destination_rotation = Quaternion.LookRotation(destinationAttackPoint.position);
            loc_tile_position = Quaternion.Lerp(loc_origin_rotation, loc_destination_rotation, (par_tile_index - 1) / (float) (tileCount + (-4 + 1)));
        }

        return loc_tile_position;
    }

    private void EndBaseBattle(GameUnit par_game_unit)
    {
        foreach (Battle loc_battle in battleTable)
        {
            if (loc_battle.IsBaseBattle() && loc_battle.IsFighter(par_game_unit))
            {
                battleTable.Remove(loc_battle);
                par_game_unit.SetBattle(null);
                break;
            }
        }
    }

    private void StartBaseBattle(GameUnit par_game_unit)
    {
        PlayerBase loc_opponent_base = GameManager.gameManager.GetEnemyBase(par_game_unit.GetPlayerBase());
        Battle loc_battle = new Battle(par_game_unit, loc_opponent_base);

        par_game_unit.SetBattle(loc_battle);

        battleTable.Add(loc_battle);
    }

    private void StartUnitBattle(GameUnit par_game_unit, GameUnit par_opponent_game_unit)
    {
        if (par_opponent_game_unit.GetCurrentState() == GameUnit.UnitState.NONE)
        {
            Battle loc_battle = new Battle(par_game_unit, par_opponent_game_unit);

            par_game_unit.SetBattle(loc_battle);
            par_opponent_game_unit.SetBattle(loc_battle);

            battleTable.Add(loc_battle);
        }
    }

    private int GetNextTileIndex(int par_current_tile_index, Transform par_spawn_point)
    {
        int loc_next_tile_index = par_current_tile_index;

        if (par_spawn_point == originSpawnPoint)
        {
            loc_next_tile_index += 1;
        }
        else
        {
            loc_next_tile_index -= 1;
        }

        return loc_next_tile_index;
    }

    private bool IsNextTileEmpty(int par_current_tile_index, Transform par_spawn_point)
    {
        bool loc_is_next_tile_empty = false;
        int loc_next_tile_index = GetNextTileIndex(par_current_tile_index, par_spawn_point);

        loc_is_next_tile_empty = tileTable[loc_next_tile_index] == null;

        return loc_is_next_tile_empty;
    }

    private int GetUnitTileIndex(GameUnit par_game_unit)
    {
        int loc_unit_tile_index = -1;

        foreach (KeyValuePair<int, GameUnit> loc_key_value_pair in tileTable)
        {
            if (loc_key_value_pair.Value == par_game_unit)
            {
                loc_unit_tile_index = loc_key_value_pair.Key;
                break;
            }
        }

        return loc_unit_tile_index;
    }

    public bool IsAttacked( Transform par_spawn_point )
    {
        bool loc_is_attacked = false;
        int loc_attack_point_index = GetEnemyAttackPointIndex(par_spawn_point);
        GameUnit loc_game_unit_on_attack_point = tileTable[ loc_attack_point_index ];

        if ((loc_game_unit_on_attack_point != null) && (loc_game_unit_on_attack_point.GetPlayerBase() != this))
        {
            loc_is_attacked = true;
        }

        return loc_is_attacked;
    }
}