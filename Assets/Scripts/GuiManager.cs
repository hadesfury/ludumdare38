﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine.UI;

public class GuiManager : MonoBehaviour
{
    [Header("Screens")]
    [SerializeField] private RectTransform titleScreenUi;
    [SerializeField] private RectTransform ingameScreenUi;
    [SerializeField] private RectTransform pauseScreenUi;
    [SerializeField] private RectTransform gameOverScreenUi;

    [Header("Buttons")]
    [SerializeField] private List<Button> laneButtonTable;
    [SerializeField] private List<Button> unitTypeButtonTable;
    [SerializeField] private List<TMP_Text> unitTypeButtonCostTextTable;

    [Header("Upgrades")]
    [SerializeField] private Button unlockMediumUnitButton;
    [SerializeField] private TMP_Text unlockMediumUnitButtonCostText;
    [SerializeField] private Button unlockHeavyUnitButton;
    [SerializeField] private TMP_Text unlockHeavyUnitButtonCostText;

    [Header("Misc")]
    [SerializeField] private TMP_Text playerResourceCountText;
    [SerializeField] private RectTransform victoryPanel;
    [SerializeField] private RectTransform defeatPanel;
    [SerializeField] private RectTransform drawPanel;
    [SerializeField] private RectTransform spawnButtonPanel;
    [SerializeField] private RectTransform unitTypeButtonPanel;

    private int selectedLaneIndex = -1;


    private void Awake()
    {
        for (int loc_lane_index = 0; loc_lane_index < GameManager.gameManager.GetLaneCount(); loc_lane_index++)
        {
            int loc_current_lane_index = loc_lane_index;
            Button loc_current_lane_button = laneButtonTable[loc_lane_index];

            loc_current_lane_button.onClick.AddListener(() => OnSpawnLaneButtonClick(loc_current_lane_index));
        }

        Array loc_loc_unit_type_table = Enum.GetValues(typeof(UnitType));

        for (int loc_unit_type_index = 0; loc_unit_type_index < loc_loc_unit_type_table.Length; loc_unit_type_index++)
        {
            UnitType loc_current_unit_type_index = (UnitType) loc_loc_unit_type_table.GetValue(loc_unit_type_index);
            Button loc_current_lane_button = unitTypeButtonTable[loc_unit_type_index];

            loc_current_lane_button.onClick.AddListener(() => OnUnitTypeButtonClick(loc_current_unit_type_index));
        }

        unitTypeButtonPanel.gameObject.SetActive(false);

        titleScreenUi.gameObject.SetActive(false);
        ingameScreenUi.gameObject.SetActive(false);

        unlockMediumUnitButton.onClick.AddListener(() => GameManager.gameManager.GetHumanPlayer().UnlockMediumUnit());
        unlockHeavyUnitButton.onClick.AddListener(() => GameManager.gameManager.GetHumanPlayer().UnlockHeavyUnit());
    }

    private void OnSpawnLaneButtonClick(int par_lane_index)
    {
        selectedLaneIndex = par_lane_index;

        foreach (Button loc_button in laneButtonTable)
        {
            loc_button.interactable = false;
        }

        unitTypeButtonPanel.gameObject.SetActive(true);
    }

    private void OnUnitTypeButtonClick(UnitType par_unit_type)
    {
        Player loc_human_player = GameManager.gameManager.GetHumanPlayer();

        loc_human_player.SpawnUnitOnLane(selectedLaneIndex, par_unit_type);

        ShowLaneButtons();
    }

    private void ShowLaneButtons()
    {
        foreach (Button loc_button in laneButtonTable)
        {
            loc_button.interactable = true;
        }

        selectedLaneIndex = -1;

        unitTypeButtonPanel.gameObject.SetActive(false);
    }

    private void Update()
    {
        switch (GameStateManager.gameStateManager.GetCurrentGameState())
        {
            case GameStateManager.GameState.MENU:
            {
                titleScreenUi.gameObject.SetActive(true);
                ingameScreenUi.gameObject.SetActive(false);
                pauseScreenUi.gameObject.SetActive(false);
                gameOverScreenUi.gameObject.SetActive(false);
                break;
            }
            case GameStateManager.GameState.PAUSE_MENU:
            {
                titleScreenUi.gameObject.SetActive(false);
                ingameScreenUi.gameObject.SetActive(false);
                pauseScreenUi.gameObject.SetActive(true);
                gameOverScreenUi.gameObject.SetActive(false);
                break;
            }
            case GameStateManager.GameState.INGAME:
            {
                titleScreenUi.gameObject.SetActive(false);
                ingameScreenUi.gameObject.SetActive(true);
                pauseScreenUi.gameObject.SetActive(false);
                gameOverScreenUi.gameObject.SetActive(false);

                Player loc_human_player = GameManager.gameManager.GetHumanPlayer();

                playerResourceCountText.SetText(loc_human_player.GetResourceCount().ToString("F0"));

                foreach (TMP_Text loc_unit_type_button_cost_text in unitTypeButtonCostTextTable)
                {
                    loc_unit_type_button_cost_text.SetText("Cost:" + GameManager.gameManager.GetUnitCost());
                }

                unlockMediumUnitButtonCostText.SetText("Cost:" + loc_human_player.GetCurrentUpgradeCost());
                unlockHeavyUnitButtonCostText.SetText("Cost:" + loc_human_player.GetCurrentUpgradeCost());

                unlockMediumUnitButton.interactable = !loc_human_player.GetIsMediumUnitUnlocked();
                unlockHeavyUnitButton.interactable = !loc_human_player.GetIsHeavyUnitUnlocked();


                if (selectedLaneIndex == -1)
                {
                    if (Input.GetKeyUp(KeyCode.Alpha1))
                    {
                        laneButtonTable[0].onClick.Invoke();
                    }
                    else if (Input.GetKeyUp(KeyCode.Alpha2))
                    {
                        laneButtonTable[1].onClick.Invoke();
                    }
                    else if (Input.GetKeyUp(KeyCode.Alpha3))
                    {
                        laneButtonTable[2].onClick.Invoke();
                    }
                    else if (Input.GetKeyUp(KeyCode.Alpha4))
                    {
                        laneButtonTable[3].onClick.Invoke();
                    }
                    else if (Input.GetKeyUp(KeyCode.Escape))
                    {
                        GameStateManager.gameStateManager.ChangeState(GameStateManager.GameState.PAUSE_MENU);
                    }
                }
                else
                {
                    Player loc_player = GameManager.gameManager.GetHumanPlayer();

                    unitTypeButtonTable[1].interactable = loc_player.GetIsMediumUnitUnlocked();
                    unitTypeButtonTable[2].interactable = loc_player.GetIsHeavyUnitUnlocked();

                    if (Input.GetKeyUp(KeyCode.Alpha1))
                    {
                        unitTypeButtonTable[0].onClick.Invoke();
                    }
                    else if (Input.GetKeyUp(KeyCode.Alpha2))
                    {
                        if (loc_player.GetIsMediumUnitUnlocked())
                        {
                            unitTypeButtonTable[1].onClick.Invoke();
                        }
                    }
                    else if (Input.GetKeyUp(KeyCode.Alpha3))
                    {
                        if (loc_player.GetIsHeavyUnitUnlocked())
                        {
                            unitTypeButtonTable[2].onClick.Invoke();
                        }
                    }
                    else if (Input.GetKeyUp(KeyCode.Escape))
                    {
                        ShowLaneButtons();
                    }
                }

                break;
            }
            case GameStateManager.GameState.GAME_OVER:
            {
                titleScreenUi.gameObject.SetActive(false);
                ingameScreenUi.gameObject.SetActive(false);
                pauseScreenUi.gameObject.SetActive(false);
                gameOverScreenUi.gameObject.SetActive(true);
                break;
            }
        }
    }

    public void DisplayVictoryScreen()
    {
        victoryPanel.gameObject.SetActive(true);
        defeatPanel.gameObject.SetActive(false);
        drawPanel.gameObject.SetActive(false);
    }

    public void DisplayDefeatScreen()
    {
        victoryPanel.gameObject.SetActive(false);
        defeatPanel.gameObject.SetActive(true);
        drawPanel.gameObject.SetActive(false);
    }

    public void DisplayDrawScreen()
    {
        victoryPanel.gameObject.SetActive(false);
        defeatPanel.gameObject.SetActive(false);
        drawPanel.gameObject.SetActive(true);
    }
}
