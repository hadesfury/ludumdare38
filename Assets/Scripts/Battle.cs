﻿using UnityEngine;

public class Battle
{
    private float battleRoundRate = 1.0f;

    private readonly GameUnit fighterA;
    private readonly GameUnit fighterB;
    private readonly PlayerBase opponentBase;

    private float timeSinceLastRound;

    public Battle(GameUnit par_fighter_a, GameUnit par_fighter_b)
    {
        fighterA = par_fighter_a;
        fighterB = par_fighter_b;

        Debug.LogFormat("<color=orange>Battle started</color> {0} -> {1}", fighterA.name, fighterB.name);
    }

    public Battle(GameUnit par_fighter, PlayerBase par_opponent_base)
    {
        fighterA = par_fighter;
        opponentBase = par_opponent_base;
        Debug.LogFormat("<color=red>Base Battle started</color> {0} -> {1}", fighterA.name, opponentBase.name);
    }

    public void Update( float par_delta_time )
    {
        timeSinceLastRound += par_delta_time;

        if (timeSinceLastRound >= battleRoundRate)
        {
            if (fighterB != null)
            {
                // No attack speed
                fighterA.Attack(fighterB);
                fighterB.Attack(fighterA);
            }
            else
            {
                fighterA.Attack( opponentBase );
            }

            timeSinceLastRound = 0;
        }
    }

    public bool IsOver()
    {
        bool loc_is_over = false;

        if (IsBaseBattle())
        {
            loc_is_over = !opponentBase.gameObject.activeSelf;
        }
        else
        {
            loc_is_over = fighterA.IsDead() || fighterB.IsDead();
        }

        return loc_is_over;
    }

    public bool IsBaseBattle()
    {
        return opponentBase != null;
    }

    public bool IsFighter( GameUnit par_game_unit )
    {
        return (fighterA == par_game_unit) || ( fighterB == par_game_unit );
    }

    public void Clear()
    {
        fighterA.SetBattle( null );

        if (fighterB != null)
        {
            fighterB.SetBattle( null );
        }
    }
}
