﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using Random = UnityEngine.Random;

public class GameManager : MonoBehaviour
{
    [Serializable]
    public class UnitTypePrefab
    {
        public UnitType unitType;
        public Transform prefab;
    }
    public static GameManager gameManager;

    [SerializeField] private GuiManager guiManager;
    
    [Header("Gameplay")]
    [SerializeField] private float weaknessAttackBonus = 0.5f;
    [SerializeField] private float resourcesGenerationRate = 1.0f;
    [SerializeField] private float unitCost = 5.0f;
    [SerializeField] private float initialUpgradeCost = 100.0f;
    [SerializeField] private float upgradeCostIncreaseFactor = 2.0f;
    
    [Header("Misc")]
    [SerializeField] private List<AttackLane> attackLaneTable;
    [SerializeField] private List<PlayerBase> playerBaseTable;
    [SerializeField] private List<TMP_Text> playerBaseLifeTextTable;
    [SerializeField] private Planet planet;

    [Header("Prefabs")]
    [SerializeField] private List<UnitTypePrefab> unitTypePrefabTable;
    [SerializeField] private GameUnit gameUnitPrefab;

    [Header("Players")]
    [SerializeField] private List<Player> playerTable;

    private int currentGameUnitIndex = 0;
    private readonly List<GameUnit> instancedGameUnitTable = new List<GameUnit>();

    private void Awake()
    {
        gameManager = this;

        gameObject.AddComponent<GameStateManager>();
    }

    public void Initialise()
    {
        foreach ( GameUnit loc_game_unit in instancedGameUnitTable )
        {
            GameObject.Destroy( loc_game_unit.gameObject );    
        }

        instancedGameUnitTable.Clear();

        foreach ( AttackLane loc_attack_lane in attackLaneTable )
        {
            loc_attack_lane.Initialise();
        }

        int loc_player_index = 0;

        foreach ( PlayerBase loc_player_base in playerBaseTable )
        {
            loc_player_base.Initialise(playerTable[loc_player_index]);

            ++loc_player_index;
        }

        foreach ( Player loc_player in playerTable )
        {
            loc_player.Initialise();
        }
    }

    public float GetUnitCost()
    {
        return unitCost;
    }

    public GameUnit GetNewGameUnit( UnitType par_unit_type )
    {
        GameUnit loc_new_game_unit = GameObject.Instantiate(gameUnitPrefab);

        ++currentGameUnitIndex;

        loc_new_game_unit.name = par_unit_type + "#" + currentGameUnitIndex + " " + gameUnitPrefab.name;
        
        UnitTypePrefab loc_unit_type_prefab = null;

        foreach ( UnitTypePrefab loc_type_prefab in unitTypePrefabTable )
        {
            if (loc_type_prefab.unitType == par_unit_type)
            {
                loc_unit_type_prefab = loc_type_prefab;
                break;
            }
        }

        Transform loc_mesh_type = GameObject.Instantiate(loc_unit_type_prefab.prefab);

        loc_new_game_unit.SetUnitType(loc_unit_type_prefab.unitType, loc_mesh_type);

        instancedGameUnitTable.Add( loc_new_game_unit );

        return loc_new_game_unit;
    }

    public Planet GetPlanet()
    {
        return planet;
    }

    public float GetDamageModifier(UnitType par_attacking_unit_type, UnitType par_defending_unit_type)
    {
        float loc_damage_modifier = 0;

        if ((par_attacking_unit_type == UnitType.RED) && (par_defending_unit_type == UnitType.GREEN))
        {
            loc_damage_modifier = weaknessAttackBonus;
        }
        else if ((par_attacking_unit_type == UnitType.GREEN) && (par_defending_unit_type == UnitType.BLUE))
        {
            loc_damage_modifier = weaknessAttackBonus;
        }
        else if ((par_attacking_unit_type == UnitType.BLUE) && (par_defending_unit_type == UnitType.RED))
        {
            loc_damage_modifier = weaknessAttackBonus;
        }

        return loc_damage_modifier;
    }

    public PlayerBase GetEnemyBase(PlayerBase par_player_base)
    {
        PlayerBase loc_enemy_base = null;

        foreach (PlayerBase loc_player_base in playerBaseTable)
        {
            if (loc_player_base != par_player_base)
            {
                loc_enemy_base = loc_player_base;
            }
        }

        return loc_enemy_base;
    }
    

    private void Update()
    {
        if ( GameStateManager.gameStateManager.GetCurrentGameState() == GameStateManager.GameState.INGAME )
        {
            int loc_base_index = 0;

            foreach ( TMP_Text loc_tmp_text in playerBaseLifeTextTable )
            {
                loc_tmp_text.SetText( playerBaseTable[ loc_base_index ].GetComponent<LifeComponent>().GetCurrentLife().ToString( "F1" ) );

                ++loc_base_index;
            }

            foreach ( AttackLane loc_attack_lane in attackLaneTable )
            {
                loc_attack_lane.Progress( Time.deltaTime );
            }

            int locDeadPlayerCount = 0;
            Player locDeadPlayer = null;

            foreach ( Player loc_player in playerTable )
            {
                if ( loc_player.IsDead() )
                {
                    ++locDeadPlayerCount;
                    locDeadPlayer = loc_player;
                }
                else
                {
                    loc_player.AddResources( resourcesGenerationRate * Time.deltaTime );
                }
            }

            if ( locDeadPlayerCount > 0 )
            {
                GameStateManager.gameStateManager.ChangeState( GameStateManager.GameState.GAME_OVER );

                if ( locDeadPlayerCount == 1 )
                {
                    if ( locDeadPlayer == GetHumanPlayer() )
                    {
                        // you lose
                        guiManager.DisplayDefeatScreen();
                    }
                    else
                    {
                        // you win
                        guiManager.DisplayVictoryScreen();
                    }
                }
                else
                {
                    // draw
                    guiManager.DisplayDrawScreen();
                }
            }
        }
    }

    public AttackLane GetAttackLane(Transform par_spawn_point)
    {
        AttackLane loc_attack_lane = null;

        foreach (AttackLane loc_current_attack_lane in attackLaneTable)
        {
            if (loc_current_attack_lane.ContainsSpawnPoint(par_spawn_point))
            {
                loc_attack_lane = loc_current_attack_lane;
            }
        }

        return loc_attack_lane;
    }

    public void SpawnUnitOnLane(int par_lane_index)
    {
        //attackLaneTable[ par_lane_index ].Spawn( par_lane_index );
    }

    public Player GetHumanPlayer()
    {
        Player loc_human_player = null;

        foreach (Player loc_player in playerTable)
        {
            if (loc_player is HumanPlayer)
            {
                loc_human_player = loc_player;

                break;
            }
        }

        return loc_human_player;
    }

    public int GetLaneCount()
    {
        return attackLaneTable.Count;
    }

    public float GetInitialUpgradeCost()
    {
        return initialUpgradeCost;
    }

    public float GetUpgradeCostIncreaseFactor()
    {
        return upgradeCostIncreaseFactor;
    }
}